
<div class="container-fluid" style="margin-bottom: 5em;">
  <div class="row">
    <div class="col">
      <div class="card text-center">
        <div class="card-header">
          <div class="row">
            <div class="col">
              <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                  <a class="nav-link active" id="TickAbiertos">Abiertos</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="TickAproceso">Proceso</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="TickCerrado">Cerrado</a>
                </li>
                <!-- <li class="nav-item">
                  <a class="nav-link disabled" href="#">Disabled</a>
                </li> -->
              </ul>
            </div>
          
            <div class="form-group col">    
              <input list="PosiblesResultados"  type="text" class="form-control" id="BuscadorTick" placeholder="Buscador" autocomplete="on">
              <datalist id="PosiblesResultados"> 


              </datalist>
            </div>
<!--             <div id="suggestions"></div> -->
          </div>
        </div>
        <div class="card-body" style="width: 100%; height: 25em; overflow-y: scroll;">
          <div class="list-group" id='tickets'>
            
            <input type="hidden" name="" value="4" id="pagina"> 
          </div>
        </div>
         
      </div>
      <br>
       <!-- <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
              </li>
              <li class="page-item"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">Next</a>
              </li>
            </ul>
       </nav> -->
    </div>  

    <div class="col">
      <div class="card">
        <div class="card-header" style="text-align: justify-all;">
          Mensajes

<div class="btn-group" role="group" style="float: right; display: none;" id="divEstatus">
            <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="cambiarestaus()">
             Cambiar Estatus
            </button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" id="OpcEstatus">
              <a class="dropdown-item" onclick="cambiarestatus(2);">Proceso</a>
              <a  class="dropdown-item" data-toggle="modal" data-target="#modalcerrar">Cerrado</a>
            </div>
        </div>

        <!-- Modal -->
<div class="modal fade" id="modalcerrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Comentario final</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> 
      </div>
      <div class="modal-body">
        <textarea class="form-control" id="comentarioFinal" style="width: 100%"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="Enviarcomen" data-dismiss="modal" onclick="cambiarestatus(3);" style="display: none;">Enviar</button>
      </div>
    </div>
  </div>
</div>

          <div class="btn-group" role="group" style="float: right; display: none;" id="divAsignar">
            <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-right: 0.4em;"  onclick="UserPorAsignar()">
              Asignar
            </button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" id="Usuariosporasignar"  style="width: 100%; height: 9em; overflow-y: scroll;" >
           
            </div>
        </div> 


      <div class="btn-group" role="group" style="float: right; display: none;" id="divAsignar1" >
            <button id="btnGroupD" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-right: 0.4em;" onclick="DepartamentoAsig()">
              Departamento
            </button>
            <div class="dropdown-menu" aria-labelledby="btnGroupD" id="DepartamentoAsig"  style="width: 100%; height: 7em; overflow-y: scroll;" >
           
            </div>
        </div> 




        </div>

        <div class="card-body" style="width: 100%; height: 25em; overflow-y: scroll;" id="mensajeshisto">
          
        </div>
      </div>
      <div class="input-group mb-3" id="msjEnviar" style="display: none;">
        <textarea type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="button-addon2" id="mensjnew"></textarea>
        <div class="input-group-append">
          <button class="btn btn-primary" type="button" id="guardarmensj" ><img src="../../recursos/email.svg" width="40px"></img></button>
        </div>
      </div>
      
           
         

      <!-- <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">With textarea</span>
        </div>
        <textarea class="form-control" aria-label="With textarea"></textarea>
      </div> -->
      <!-- <div class="card">
        <div class="card-body">
          <textarea style="width: 90%;"></textarea>
        </div>
      </div> -->
    </div>
</div>

<?php 
  $busqueda=isset($_GET["var"]) ? $_GET['var'] : NULL; 
?>
<input type="hidden" id="busqdato" value="<?php echo $busqueda; ?>">
  <!-- <script type="text/javascript"> 
    var busqueda=$('#busqdato').val();
   busquedautomatica(busqueda);
</script> -->



 
<div id="volver">      
     <button type="button" class="btn btn-secondary"><a href="http://192.168.1.6:8080/soporte_postVenta/PHP/inicio/">Volver</a></button>
</div>