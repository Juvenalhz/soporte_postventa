<?php 
$hoy=date('dmo'); 
include_once ('../../default/conexion.php');
$sql="SELECT COUNT(*) FROM TK";
$resultado=pg_query($db_soporte,$sql);
$totaltickets=pg_fetch_result($resultado, 0, 0)+1;
$nuevoticket=$hoy.$totaltickets;
?>
<input type="hidden" name="totaltickets" id="totaltickets" value="<?php echo $nuevoticket; ?>">
<div class="container-fluid">

    <div class="card" id="form-crear">
        <div class="card-header">
            <strong>Nuevo Ticket</strong>
        </div>
        <div class="card-body" id="camposform">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="rif">Rif</label>
                    <input type="text" class="form-control" id="rif" placeholder="RIF" disabled="on">
                </div>
                <div class="form-group col-md-6">
                    <label for="rs">Razon Social</label>
                    <input type="text" class="form-control" id="rs" placeholder="Razon Social" disabled="on">
                </div>
            </div>
             <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="person">Persona Contacto</label>
                    <input type="text" class="form-control" id="PersonaContac" placeholder="Persona Contacto" maxlength="50" required/>
                </div>
                <div class="form-group col-md-6">
                    <label for="number1">Número telefónico 1</label>
                    <input type="tel" class="form-control phone phone_with_ddd" onkeypress="return solonumeros(event)" id="NumeroTelf1" placeholder="Número telefónico 1" minlength="15" maxlength="15" required>
                </div>
            </div><div class="form-row">
                
                <div class="form-group col-md-6">
                    <label for="number2">Número telefónico 2</label>
                    <input type="tel" class="form-control phone phone_with_ddd" onkeypress="return solonumeros(event)" id="NumeroTelf2" placeholder="Número telefónico 2" minlength="15" maxlength="15">
                </div>

                <div class="form-group col-md-6">
                    <label for="person">Correo electrónico</label>
                    <input type="email" class="form-control" id="Correo" maxlength="50" placeholder="Correo electrónico" required>
                </div>
            </div>
            

             <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Serial:
                        <select  class="form-control" name="select" id="optseriales">
                          <option value="">Seleccione serial del caso</option> 
                         
                        </select>
                  <!--   <input list="PosiblesSeriales"  type="text" class="form-control" id="selectserial">
                    <datalist id="PosiblesSeriales"> 


                    </datalist> -->
                </div>
                <div class="form-group col-md-6">
                    <label>Seleccione el motivo de la llamada:
                    <input list="PosiblesResultados"  type="text" class="form-control" id="selectmotivo">
                    <datalist id="PosiblesResultados"> 


                    </datalist>
                </div>
            </div>

            <div class="form-group">
                <label for="Motivo">Observación</label>
                <textarea class="form-control" id="Motivo" rows="4" required></textarea>
            </div>
            <div class="form-group">
                <label for="Accion">Accion</label>
                <textarea class="form-control" id="Accion" rows="4"></textarea>
            </div>
            <div id='BotonesCreadores'> 
            <div class="form-row">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="check" checked>
                    <label class="form-check-label" for="check">
                        Asignar
                    </label>
                </div>

            </div>

            <br>
            <div class="form-group" id="asig">
                <div class="form-group col-md-8">
                    <label for="asignar">Asignar A</label>
                    <select id="asignarDep" class="form-control">
                        <option value="0">Departamento/Banco</option>
                        
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <button type="button" id='CerrarTick' class="btn btn-primary" style="display: none;" disabled>Cerrar Ticket</button>
                    <button type="button" id='CrearTick' class="btn btn-primary" disabled>Crear Ticket</button>
                </div> 
                <div class="col">
                    <button type="button" id='tickClie' target="_blank" class="btn btn-primary" style="display: none;">Tickets del Cliente</button>
                </div>
            </div>
            </div>
            
        </div>
            <!-- <div class="row">
                <div class="col">
                    <div class="card">
                      <div class="card-body" style="background-color: rgba(255,0,0,0.6);">
                        Abiertos
                      </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                      <div class="card-body">
                        Proceso
                      </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                      <div class="card-body" style="background-color: rgba(0,255,0,0.6);">
                        Cerrado
                      </div>
                    </div>
                </div>
            </div> -->
    </div>

    <div class="card" id="info">
        <div class="card-header">
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Buscar Rif" id="brif" aria-label="Recipient's username" aria-describedby="button-addon2">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="button" id="buscar">Buscar</button>
                </div>
            </div>
        </div>

        <div class="card-body" id="infor">

        </div>

    </div>
    
</div>


 
<div id="volver">      
     <button type="button" class="btn btn-secondary"><a href="http://192.168.1.6:8080/soporte_postVenta/PHP/inicio/">Volver</a></button>
</div>
<script type="text/javascript">
     function solonumeros(e){
       key=e.keyCode || e.wich; 

       teclado=String.fromCharCode(key);
       numeros="0123456789";

       especiales="8-37-38-46";

       teclado_especial=false;

       for(var i in especiales){
        if (key==especiales[i]){
          teclado_especial=true;
        }
       }
       if(numeros.indexOf(teclado)==-1 && !teclado_especial){
        return false;

       }
    };


</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script>
    $('.phone_with_ddd').mask('0000-0000000');
</script>

