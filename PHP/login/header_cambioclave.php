
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" >
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="../../recursos/ico.png" />
    <link rel="stylesheet" href="../../css/bootstrap.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link href="../../css/navbar.css" rel="stylesheet">
    <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="../../css/mycss.css" rel="stylesheet">
    <link href="../../css/sticky-footer.css" rel="stylesheet">
    <link href="../../css/sweetalert2.min.css" rel="stylesheet">
    <link href="../../css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../css/sb-admin-2.css" rel="stylesheet">
    <link href="../../css/ihover.min.css" rel="stylesheet">

    <script type="text/javascript" src="../../js/sweetalert2.min.js"></script>
    <!-- <script type="text/javascript" src="../../js/bootstrap.bundle.min.js"></script> -->
    <script type="text/javascript" src="../../js/jquery-3.4.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="../js/jquery-slim.min.js"><\/script>')</script>
    <script src="../../js/popper.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>  

    <script src="../../vendor/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js" type="text/javascript"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jqueryclave.min.js"></script>
    <script src="../login/strength.js" type="text/javascript"></script>
    <script src="../login/js.js" type="text/javascript"></script>



  <title>SOPORTE_POST</title>
</head>
<body>

  <nav class="navbar navbar-dark bg-dark rounded" id="navbar">
      
        <a class="navbar-brand" href="" > <img src="../../recursos/logo-soporte-pos.png" height="80px">Soporte Post Venta</a>
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-white small"><?php echo strtoupper($_SESSION['nombre']); ?></span>
                <img class="img-profile rounded-circle" src="../../recursos/user.svg" height="40px">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <!--<a class="dropdown-item" href="http://192.168.1.6:8080/soporte_postVenta/PHP/login/cambiarclave.php">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Cambiar Clave
                </a>-->
               <!--  <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a> -->
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="../login/logout.php">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Cerrar Sesion
                </a>
              </div>
  


      
    </nav>
<?php 
include_once ('../../default/conexion.php');
$id=$_SESSION['iduser'];
$sql=pg_query($db_soporte,"SELECT * FROM usuarios WHERE usuario_id = '$id'");
$row = pg_fetch_assoc($sql);
    
      $_SESSION['usuario']=$row['usuario_nombre'];
      $_SESSION['CI']=$row['nif'];
      $_SESSION['tipo_user']=$row['tipo'];
      $_SESSION['departamento']=$row['dep'];
      $_SESSION['correo']=$row['usuario_email'];
 ?>

