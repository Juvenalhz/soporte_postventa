<input type="hidden" id="pagina" name="" value="1">
    <div class="container ">
    	
    <div class="card-header rounded border ">
    	<strong>Tabla de Departamentos/Bancos</strong>
    	
    </div>
     <div class="table-responsive rounded border ">
	<table class="table table-striped table-sm " id="dataTable1" width="100%" cellspacing="0" style="text-align: center;">
		<thead class="table-info">
			
			<tr>	
					<th width="30%">Nro</th>
					<th width="70%">Departamento / Banco</th>
					<th width="20%">opciones</th>
					
				
			</tr>
			
		</thead>
		
		<tbody id="dep">

		</tbody>
				<tfoot class="table-info">
								
			<tr>	
					<th width="30%">Nro</th>
					<th width="50%">Departamento / Banco</th>
					<th width="20%">opciones</th>
				
			</tr>
				</tfoot>

	</table>
	 </div>
          
        <div class="card-footer small text-muted rounded border ">
        	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalDep" data-whatever="@mdo">Agregar</button>
<div style="text-align: right;">
	 <strong ><?php echo "FECHA: ".date('d / m / o'); ?></strong>    
</div>
        

        </div>
        
</div>

	<!-- MODAL AGREGAR -->
	<div class="modal fade" id="ModalDep" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Nuevo Banco o Departamento</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <form>
	          <div class="form-group">
	            <label for="recipient-name" class="col-form-label">Departamento o Banco:</label>
	            <input type="text" class="form-control" id="Departamento">
	          </div>
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-primary" id="GuardarDep">Guardar</button>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- MODAL EDITAR -->
	<div class="modal fade" id="ModalEditDep" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Nuevo Banco o Departamento</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <form>
	          <div class="form-group">
	            <label for="recipient-name" class="col-form-label">Departamento o Banco:</label>
	            <input type="text" class="form-control" id="DepartamentoEdit">
	          </div>
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-primary" id="GuardarDep">Guardar</button>
	      </div>
	    </div>
	  </div>
	</div>
	 
<div id="volver">      
     <button type="button" class="btn btn-secondary"><a href="http://192.168.1.6:8080/soporte_postVenta/PHP/inicio/">Volver</a></button>
</div>