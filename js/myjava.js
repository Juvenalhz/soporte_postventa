$(document).ready(pagina);
$(function(){
$("#check").on('change', function() {
if( $(this).is(':checked') ) {
// Hacer algo si el checkbox ha sido seleccionado
	$("#asig").css("display", "inline");
	$("#CrearTick").css("display", "inline");
	$("#CerrarTick").css("display", "none");
} else {
// Hacer algo si el checkbox ha sido deseleccionado
	$("#asig").css("display", "none");
	$("#CerrarTick").css("display", "inline");
	$("#CrearTick").css("display", "none");
}
});
$("#asignarDep").on('change', function() {

    $('#CrearTick').removeAttr("disabled");
});
$("#buscar").on('click', function() {
var rif = $("#brif").val();
if (rif == '' || rif == null) {
$("#brif").addClass(" is-invalid");
}
else {
	$.ajax({
		type:'POST',
		url:'clientes.php',
		data:'rif='+rif,
		success: function(e){
			if (e==0) {
				$("#brif").addClass(" is-invalid");
				$('#infor').hide();
				$("#rif").val('');
				$("#rs").val('');
				$("#PersonaContac").val('');
				$("#NumeroTelf1").val('');
				$("#NumeroTelf2").val('');
				$("#Correo").val('');
				Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'No existe en Intelipunto.',
						showConfirmButton: false,
						timer: 3000
					})
			} else {
			$('#infor').show();
			$('#infor').html(e);
			}
		}
	});
}});
// $("#clienteposibles").on('click', function() {
// 	alert();
// })
$('#GuardarDep').click(function(){
	var codigo = $('#Codigo').val();
	var departamento = $('#Departamento').val();
	$.ajax({
		type:'POST',
		url:'AgregarNuevoDep.php',
		data:{Codigo:codigo, Departamento:departamento},
			success: function(data){
				if (data==1) {
					Swal.fire({
			    		position: 'center-top', 
						icon: 'success',
						title: 'Agregado con éxito.',
						showConfirmButton: false,
						timer: 1500
					});
					pagina();
					 $("#ModalDep").modal('hide');
				}else{
					Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'No se pudo guardar.',
						showConfirmButton: false,
						timer: 1500
					});
				}
			}
	});
});
$('#CrearTick').click(function(){
	var serial=$('#optseriales').val();
	var RIF= $('#rif').val();
	var razonsocial= $('#rs').val();
	var observacion= $('#Motivo').val();
	var accion= $('#Accion').val();
	var dep= $('#asignarDep').val();
	var numeroticket= $('#totaltickets').val();
	var PersonaContac= $('#PersonaContac').val();
	var NumeroTelf1= $('#NumeroTelf1').val();
	var NumeroTelf2= $('#NumeroTelf2').val();
	var Correo= $('#Correo').val();
	var selectmotivo= $('#selectmotivo').val();
	if (Correo!="" && !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(Correo))){
      Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'La dirección de email es incorrecta.',
						showConfirmButton: false,
						timer: 2500
    })
  } 
	else if (selectmotivo=='' || PersonaContac=='' || NumeroTelf1=='' || Correo=='' || dep=='' || serial=='') {
		Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'Campos obligatorios vacios.',
						showConfirmButton: false,
						timer: 2500
	});
		if(selectmotivo==''){
			$("#selectmotivo").addClass(" is-invalid");
		} else {
			$("#selectmotivo").removeClass(" is-invalid");
		}
		if(serial=='' || serial=='0'){
			$("#optseriales").addClass(" is-invalid");
		}else {
			$("#optseriales").removeClass(" is-invalid");
		}
		 if (PersonaContac=='') {
			$("#PersonaContac").addClass(" is-invalid");
		} else {
			$("#PersonaContac").removeClass(" is-invalid");
		}
		if (NumeroTelf1=='') {
			$("#NumeroTelf1").addClass(" is-invalid");
		}else {
			$("#NumeroTelf1").removeClass(" is-invalid");
		}
		if (Correo=='') {
			$("#Correo").addClass(" is-invalid");
		}else {
			$("#Correo").removeClass(" is-invalid");
		}
	}else if(NumeroTelf1.length<12){
		Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'El número telefónico no esta completo.',
						showConfirmButton: false,
						timer: 2500
	});
		$("#NumeroTelf1").addClass(" is-invalid");
	}
	 else { 
		$("#selectmotivo").removeClass(" is-invalid");
		$("#PersonaContac").removeClass(" is-invalid");
		$("#NumeroTelf1").removeClass(" is-invalid");
		$("#Correo").removeClass(" is-invalid");
		$("#optseriales").removeClass(" is-invalid");
	$.ajax({
		type:'POST',
		url:'Creacion.php',
		data:{	RIF:RIF,
				RS: razonsocial,
				MT: observacion,
				ACC: accion,
				Dep: dep,
				NT: numeroticket,
				PC: PersonaContac,
				NTF1: NumeroTelf1,
				NTF2: NumeroTelf2,
				ML: Correo,
				SM: selectmotivo,
				serial:serial},
		success: function(data){
			if (data!=0 &&  data!=3) {
					Swal.fire(
					  'Ticket creado con éxito',
					  'Número de ticket es: '+data,
					  'success'
					)
				$('input[type="text"]').val('');
				$('input[type="tel"]').val('');
				$('input[type="email"]').val('');
				$('textarea').val('');
				$('#optseriales').val(0);
				//$("#check").prop('checked', false);
				// $("#asig").css("display", "none");
				// $("#CerrarTick").css("display", "inline");
        			$("#tickClie").css("display", "none");
     
        			$("#CrearTick").prop('disabled', true);
        			$("#asignarDep").val(0);
       			$('#infor').hide();
       			// enviarmensaje(PersonaContac,razonsocial,numeroticket,motivo,accion,'1',Correo);
				}else if (data==3){
					Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'Ese cliente tiene un ticket abierto con ese serial.',
						showConfirmButton: false,
						timer: 2500
				});
				}else{
					Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'Error, no fue posible cerrar este ticket.',
						showConfirmButton: false,
						timer: 1500
				});
			}
		}
	});
	}
});
$('#CerrarTick').click(function(){
	var serial=$('#optseriales').val();
	var RIF= $('#rif').val();
	var razonsocial= $('#rs').val();
	var observacion= $('#Motivo').val();
	var accion= $('#Accion').val();
	var dep= $('#asignarDep').val();
	var numeroticket= $('#totaltickets').val();
	var PersonaContac= $('#PersonaContac').val();
	var NumeroTelf1= $('#NumeroTelf1').val();
	var NumeroTelf2= $('#NumeroTelf2').val();
	var Correo= $('#Correo').val();
	var selectmotivo= $('#selectmotivo').val();
	if (Correo!="" && !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(Correo))){
      Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'La dirección de email es incorrecta.',
						showConfirmButton: false,
						timer: 2500
    })
  } 
	else if (selectmotivo=='' || PersonaContac=='' || NumeroTelf1=='' || Correo=='' || dep=='' || serial=='') {
		Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'Campos obligatorios vacios.',
						showConfirmButton: false,
						timer: 2500
	});
		if(selectmotivo==''){
			$("#selectmotivo").addClass(" is-invalid");
		} else {
			$("#selectmotivo").removeClass(" is-invalid");
		}
		if(serial=='' || serial=='0'){
			$("#optseriales").addClass(" is-invalid");
		}else {
			$("#optseriales").removeClass(" is-invalid");
		}
		 if (PersonaContac=='') {
			$("#PersonaContac").addClass(" is-invalid");
		} else {
			$("#PersonaContac").removeClass(" is-invalid");
		}
		if (NumeroTelf1=='') {
			$("#NumeroTelf1").addClass(" is-invalid");
		}else {
			$("#NumeroTelf1").removeClass(" is-invalid");
		}
		if (Correo=='') {
			$("#Correo").addClass(" is-invalid");
		}else {
			$("#Correo").removeClass(" is-invalid");
		}
	}else if(NumeroTelf1.length<12){
		Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'El numero telefónico no esta completo.',
						showConfirmButton: false,
						timer: 2500
	});
		$("#NumeroTelf1").addClass(" is-invalid");
	}
	 else { 
		$("#selectmotivo").removeClass(" is-invalid");
		$("#PersonaContac").removeClass(" is-invalid");
		$("#NumeroTelf1").removeClass(" is-invalid");
		$("#Correo").removeClass(" is-invalid");
		$("#optseriales").removeClass(" is-invalid");	
	$.ajax({
		type:'POST',
		url:'Cerrarticket.php',
		data:{	RIF:RIF,
				RS: razonsocial,
				MT: observacion,
				ACC: accion,
				Dep: dep,
				NT: numeroticket,
				PC: PersonaContac,
				NTF1: NumeroTelf1,
				NTF2: NumeroTelf2,
				ML: Correo,
				SM: selectmotivo,
				serial:serial},
		success: function(data){
			if (data!=0) {
					Swal.fire(
					  'Ticket Cerrado',
					  'Número de ticket es: '+data,
					  'success'
					)
				$('input[type="text"]').val('');
				$('input[type="tel"]').val('');
				$('input[type="email"]').val('');
				$('textarea').val('');
				$('#optseriales').val(0);
				// $("#check").prop('checked', false);
				// $("#asig").css("display", "none");
				// $("#CerrarTick").css("display", "inline");
       			$("#tickClie").css("display", "none");
       			$("#CerrarTick").prop('disabled', true);
       			$('#infor').hide();
       			// enviarmensaje(PersonaContac,razonsocial,numeroticket,motivo,accion,'2',Correo);
				}else{
					Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'Error, no fue posible cerrar este ticket.',
						showConfirmButton: false,
						timer: 1500
					});
				}
			}
		});
	}
});
$('#GuardarUser').click(function(){
	var documento= $('#documentouser').val();
	var nombre= $('#nombreuser').val();
	var apellido= $('#apellidouser').val();
	var usuario= $('#usuarionew').val();
	var tipouser= $('#TipoUser').val();
	var correouser= $('#correouser').val();
	var Departamento= $('#DepUser').val();  
	var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
	if (documento.length == 0 || documento.length < 4) {
		$('#documentouser').addClass("border border-danger");
		$('#documentouser').focus();
		$('#errordocumento').show();
	} else if (nombre.length == 0){
		$('#documentouser').removeClass("border border-danger"); 
		$('#errordocumento').hide();
		$('#nombreuser').addClass("border border-danger");
		$('#nombreuser').focus();
		$('#errornombre').show();
	} else if (apellido.length == 0){
		$('#nombreuser').removeClass("border border-danger"); 
		$('#errornombre').hide();
		$('#apellidouser').addClass("border border-danger");
		$('#apellidouser').focus();
		$('#errorapellido').show();
	}else if (usuario.length == 0){
		$('#apellidouser').removeClass("border border-danger"); 
		$('#errorapellido').hide();
		$('#usuarionew').addClass("border border-danger");
		$('#usuarionew').focus();
		$('#errorusuario').show();
	}else if (correouser.length== 0 || caract.test(correouser) == false){
		$('#usuarionew').removeClass("border border-danger");
		$('#errorusuario').hide();
		$('#correouser').addClass("border border-danger");
		$('#correouser').focus()
		$('#errorcorreo').show();
	}else if (tipouser == 0){ 
		$('#correouser').removeClass("border border-danger");  
		$('#errorcorreo').hide();
		$('#TipoUser').addClass("border border-danger");
		$('#TipoUser').focus();	
		$('#errortipou').show();
	}else if (Departamento == 0){
		$('#TipoUser').removeClass("border border-danger");  
		$('#errortipou').hide();
		$('#DepUser').addClass("border border-danger");
		$('#DepUser').focus();
		$('#errordep').show();
	} else { 
		$('#DepUser').removeClass("border border-danger");  
		$('#errornombre').hide();
	$.ajax({
		type:'POST',
		url:'crearuser.php',
		data:{	documento:documento,
				nombre: nombre,
				apellido: apellido,
				usuario: usuario,
				tipouser: tipouser,
				correouser: correouser,
				Departamento: Departamento},
		success: function(data){
			if (data==1) {
					Swal.fire({
			    		position: 'center-top', 
						icon: 'success',
						title: 'Usuario Creado Exitosamente.',
						showConfirmButton: false,
						timer: 2000
					});
                      pagina();
					 $("#ModalUser").modal('hide');
				}else if (data==2){
					Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'Intentas crear un usuario que ya existe.',
						showConfirmButton: false,
						timer: 2200
					});
				}else{
					Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'Error, no fue posible crear a este usuario.',
						showConfirmButton: false,
						timer: 1500
					});
				}
		}
	});
	}
});
$('#aggUser').click(function(){
	$("#EditarUser").css("display", "none");
    $("#GuardarUser").css("display", "inline");
    $('#ModalLabeluser').html('Nuevo Usuario');
    $("#formularioUser")[0].reset();
	$.ajax({
		url:'../crear/SeleccionDep.php',
		success: function(data){
			$('#DepUser').html(data);
		}
	});
	$.ajax({
		url:'seleccion_tipo_usuario.php',
		success: function(data){
			$('#TipoUser').html(data);
		}
	});
});
 $('#EditarUser').click(function(){
 	$.ajax({
 		type:'POST',
 		data:{
 			documento: $('#documentouser').val(),
 			nombre: $('#nombreuser').val(),
 			apellido: $('#apellidouser').val(),
 			usuario: $('#usuarionew').val(),
 			correo: $('#correouser').val(),
 			TipoUser: $('#TipoUser').val(),
 			TipoDep: $('#DepUser').val(),
 			id: $('#iduser').val()
 		},
 		url: 'editarusuario.php',
 		success:function(e){
 			Swal.fire({
			    		position: 'center-top', 
						icon: 'success',
						title: 'Usuario editado Exitosamente.',
						showConfirmButton: false,
						timer: 2000
					});
 			 		pagina();
					 $("#ModalUser").modal('hide');
 		}
 	})
 });
	$('#TickAbiertos').click(function(){
		ticketsAbiertos();
		$("#divEstatus").hide();
		$("#divAsignar").hide();
		$("#divAsignar1").hide();
		$("#mensajeshisto").empty();
	});

	$('#TickAproceso').click(function(){
		var buscador = $( "#BuscadorTick" ).val();
		$( "#TickAbiertos" ).removeClass( "active" );
		$( "#TickCerrado" ).removeClass( "active" );
		$( "#TickAproceso" ).addClass( "active" );
		$("#divEstatus").hide();
		$("#divAsignar").hide();
		$("#divAsignar1").hide();
		$('#msjEnviar').hide();
		$("#mensajeshisto").empty();
		if (buscador) {
			var variable=1;
			$.ajax({
			type: 'POST',
			url: 'ticketsProceso.php',
			data:{variable: variable,
					NT: buscador},
			success: function(e){
				$('#tickets').html(e);
			}
		});
		} else {	
			var variable=2;
		$.ajax({
			type: 'POST',
			url: 'ticketsProceso.php',
			data:{variable: variable},
			success: function(e){
				$('#tickets').html(e);
			}
		});
		}
	});
	$('#TickCerrado').click(function(){
		var buscador = $( "#BuscadorTick" ).val();
		$( "#TickAbiertos" ).removeClass( "active" );
		$( "#TickAproceso" ).removeClass( "active" );
		$( "#TickCerrado" ).addClass( "active" );
		$("#divEstatus").hide();
		$("#divAsignar").hide();
		$("#divAsignar1").hide();
		$("#mensajeshisto").empty();
		//$("#msjEnviar").css("display", "none");
		$('#msjEnviar').hide();
		if (buscador) {
			var variable=1;
			$.ajax({
			type: 'POST',
			url: 'ticketsCerrado.php',
			data:{variable: variable,
					NT: buscador},
			success: function(e){
				$('#tickets').html(e);
			}
		});
		} else {	
			var variable=2;
		$.ajax({
			type: 'POST',
			url: 'ticketsCerrado.php',
			data:{variable: variable},
			success: function(e){
				$('#tickets').html(e);
			}
		});
		}
		// $.ajax({
		// 	type: 'POST',
		// 	url: 'ticketsCerrado.php',
		// 	success: function(e){
		// 		$('#tickets').html(e);
		// 	}
		// });
	});

	$('#selectmotivo').keyup(function(){
			var dato = $('#selectmotivo').val();
			$.ajax({
			type: 'POST',
			url: 'selectmotivo.php',
			data:{dato: dato},
			success: function(e){
				$('#PosiblesResultados').html(e);
			}
		});
	}); 

	$('#guardarmensj').click(function(){
		$.ajax({
			type: 'POST',
			url: 'nuevoMensaje.php',
			data: {	mensaje: $('#mensjnew').val(),
					id:$('#idtick').val(),
					asignado:$('#asignadoTick').val(),
					estatus:$('#estatusTick').val()},
			success: function(data){
				if (data==1) {					
                      pagina();
                      $('#mensjnew').val('');
                      $("#mensajeshisto").animate({ scrollTop: $('#mensajeshisto').prop("scrollHeight")}, 1000);
					}else{
					Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'No fue posible cargar su respuesta.',
						showConfirmButton: false,
						timer: 1500
					});
				}
			}
		});
	});
 	 $("#comentarioFinal").on('keyup', function(){
 	 	var campo = $('#comentarioFinal').val();
 	 	if (campo=='') {
 	 		$("#Enviarcomen").hide();
 	 	} else {
		$("#Enviarcomen").show();
 	 	}
	});

	// $("#optseriales").click(function(){
 	 	
	// }); 
 $('#divAbierto').click(function(){	
 	window.location="http://192.168.1.74/intelipunto?var=";
  });
 $('#divCerrado').click(function(){	
 	window.location="http://192.168.1.6:8080/soporte_postVenta/PHP/mis_tickets/index.php";
  });
   $('#divProceso').click(function(){	
 	window.location="http://192.168.1.6:8080/soporte_postVenta/PHP/mis_tickets/index.php";
 	ticketsAbiertos();
  });
});
function infodetalles(){
	var url = "../crear/busqueda.php"
var rif= $('#rif').val();
	$.ajax({
		type:'POST',
		url:url,
		data:'rif='+rif,
			success:function(e){
				if (e==0) {
					Swal.fire({
					  icon: 'error',
					  title: 'Error',
					  text: 'Este cliente no se encuentra en la base de datos.',
					 // footer: '<a href>Why do I have this issue?</a>'
					})
				}
			 else 
				{
				//$('#infor').show();
				$('#infocliente').html(e);
			}
		}
	});
};


function pagina(){
var pagina = $('#pagina').val();
if (pagina==1) {
 	$.ajax({
		type:'POST',
		url:'paginar.php',
			success: function(data){
			
				
					$('#dep').html(data);
					$('#dataTable1').DataTable();
			
			}
	});

} else if (pagina==2){
	$.ajax({
		type:'POST',
		url:'paginaruser.php',
			success: function(data){
			
				
					$('#tablauser').html(data);
			        $('#dataTable').DataTable();
			}
	});
} else if (pagina==3){
	$.ajax({
		type:'POST',
		url:'paginar.php',
			success: function(data){
					$('#ideas').html(data);
			    
			}
	});
}else if (pagina==4){
	ticketsAbiertos();
}
else if (pagina==5){
	var id=$('#idtick').val();
	 MostrarHistorialMjs(id);
}

return false;
}
function eliminard(id){

Swal.fire({
  title: 'Estas Seguro',
  text: "Que desea eliminar?",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Eliminar!'
}).then((result) => {
  if (result.value) {
	 	$.ajax({
		type:'POST',
		data:{id:id},
		url:'eliminar.php',
			success: function(data){
			if (data==1) {
					Swal.fire({
			    		position: 'center-top', 
						icon: 'success',
						title: 'Eliminado con éxito.',
						showConfirmButton: false,
						timer: 1500
					});
					pagina();
					
					
				}else{
					Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'No se pudo Borrar.',
						showConfirmButton: false,
						timer: 1500
					});
				}
							
			}
	});
  }
})


}

function eliminarUsuaio(id){

Swal.fire({
  title: '¿Estas Seguro',
  text: "Que desea eliminar a este usuario?",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Eliminar!'
}).then((result) => {
  if (result.value) {
	 	$.ajax({
		type:'POST',
		data:{id:id},
		url:'EliminarUsuario.php',
			success: function(data){
			if (data==1) {
					Swal.fire({
			    		position: 'center-top', 
						icon: 'success',
						title: 'Eliminado con éxito.',
						showConfirmButton: false,
						timer: 1500
					});
					pagina();
					
					
				}else{
					Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'No se pudo Borrar.',
						showConfirmButton: false,
						timer: 1500
					});
				}
							
			}
	});
  }
})
}
function EditarUsuario(id){
	$('input[type="text"]').val('');
	$("#ModalUser").modal('show');
 	$("#GuardarUser").css("display", "none");
    $("#EditarUser").css("display", "inline");
    $('#iduser').val(id);
 	$.ajax({
	url:'../crear/SeleccionDep.php',
	success: function(data){
		$('#DepUser').html(data);
		$.ajax({
			url:'seleccion_tipo_usuario.php',
			success: function(data){
			
			$('#TipoUser').html(data);
		}
	});
	 $.ajax({
    	type: 'POST',
    	data:{id:id},
    	url:'consultaUsuario.php',
    	success:function(e){
    		var array = jQuery.parseJSON(e);
    		$('#TipoUser option:contains('+ array.tipo_usuario +')').attr('selected',true);
    		//$('#DepUser option[value='+ array.iddep +']').attr('selected',true);
    		$('#documentouser').val(array.nif);
    		$('#nombreuser').val(array.nombre);
    		$('#apellidouser').val(array.apellido);
    		$('#usuarionew').val(array.usuario_nombre);
    		$('#correouser').val(array.usuario_email);
    		
    		//$('#TipoUser option[value='+ array.tipo +']').attr('selected',true);
    		//$('#TipoUser').append('<option value="opcion_nueva_1" selected="selected">Opción nueva 1</option>');
    		
    		 $('#DepUser option:contains('+ array.nombredep +')') .attr('selected',true);    	
    	}
    });
		}
	});
};
function ResetPass(id){
	Swal.fire({
  title: '¿Estas Seguro',
  text: "Que desea reiniciar la clave de este usuario?",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Reestablecer'
}).then((result) => {
  if (result.value) {
	 	$.ajax({
		type:'POST',
		data:{id:id},
		url:'resetpass.php',
			success: function(data){
			if (data==1) {
					Swal.fire({
			    		position: 'center-top', 
						icon: 'success',
						title: 'Realizado con éxito.',
						showConfirmButton: false,
						timer: 1500
					});
					pagina();
				}else{
					Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'No se pudo reiniciar su clave.',
						showConfirmButton: false,
						timer: 1500
					});
				}
			}
	});
  }
});
};

function mostrarInfo(id){
	$("#ideas").fadeToggle('slow', 'swing');
	$("#info-idea").fadeIn('slow', 'swing');
	 $.ajax({
		type:'POST',
		url:'detalle.php',
		data:{id:id},
			success: function(data){
				$('#info-idea').html(data);
			}
	});
};
function MostrarHistorialMjs(id){
	 var ventanaCerradaSelect = document.getElementById("TickCerrado").className;
	if (ventanaCerradaSelect == 'nav-link active') {
		$('#divEstatus').hide();
		$('#divAsignar').hide();
		$('#divAsignar1').hide();
		$('#msjEnviar').hide();
	} else {
		$('#divEstatus').show();
		$('#divAsignar').show();
		$('#divAsignar1').show();
		$('#msjEnviar').show();
	}
	 $.ajax({
		type:'POST',
		url:'mostrarMensajes.php',
		data:{id:id},
			success: function(data){
			$('#mensajeshisto').html(data);
			}
	});
};
function atrasb(){
$("#ideas").fadeToggle('slow', 'swing');
$("#info-idea").fadeOut('slow', 'swing');
}
function ticketsAbiertos(){
	var buscador =  $("#BuscadorTick").val();
		$( "#TickAbiertos" ).addClass( "active" );
		$( "#TickAproceso" ).removeClass( "active" );
		$( "#TickCerrado" ).removeClass( "active" );
		$('#msjEnviar').hide();
		$("#mensajeshisto").empty();
	if (buscador) {
			var variable=1;
			$.ajax({
			type: 'POST',
			url: 'ticketsAbiertos.php',
			data:{variable: variable,
					NT: buscador},
			success: function(e){
				$('#tickets').html(e);
			}
		});
		} else {	
			var variable=2;
		$.ajax({
			type: 'POST',
			url: 'ticketsAbiertos.php',
			data:{variable: variable},
			success: function(e){
				$('#tickets').html(e);
			}
		});
		}	// $.ajax({
		// 	type: 'POST',
		// 	url: 'ticketsAbiertos.php',
		// 	success: function(e){
		// 		$('#tickets').html(e);
		// 	}
		// });
	}
function modifb(){
   	$("#guardarb").fadeToggle("fast", "swing");
	$("#modifb").prop('disabled', false);
}
function guardarbc(id){
   var solu = $("#modifb").val();
   if (solu != '') {
		$.ajax({
			type: 'POST',
			url: 'updatebc.php',
			data:{id:id,solu:solu},
			success: function(e){
				if (e==1) {
					Swal.fire({
			    		position: 'center-top', 
						icon: 'success',
						title: 'Realizado con éxito.',
						showConfirmButton: false,
						timer: 1500
					});


				}else{

					Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'No se pudo actualizar con exito',
						showConfirmButton: false,
						timer: 1500
					});
				}
			}
		});
	}else{

   		Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'No puede Guardar una solucion vacia',
						showConfirmButton: false,
						timer: 1500
					});
   }
}

function cambiarestatus(estatus){
	if (estatus==2) {

	$.ajax({
		type:"POST",
		url:"cambiarestatus.php",
		data:{estatus: estatus,
				id : $('#idtick').val()},
		success: function(data){
			if (data==1) {
				Swal.fire({
				  title: 'Seguro desea cambiar el estatus de este ticket?',
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si'
				}).then((result) => {
				  if (result.value) {
				    Swal.fire(
				      'Estatus cambiado con exito.',
				      '',
				      'success'
				    )
				pagina();
              	ticketsAbiertos();
              	$('#TickAproceso').click();
				  }
				})	
			}		
		}
	});
	} else if (estatus==3) {
		var comentario = $('#comentarioFinal').val();
		$.ajax({
		type:"POST",
		url:"cambiarestatus.php",
		data:{estatus: estatus,
				id : $('#idtick').val(),
				comentario: comentario},
		success: function(data){
			if (data==1) {
					  pagina();
                      ticketsAbiertos();
                      	$('#TickCerrado').click();

			}	
		}
	});
	}
};

function UserPorAsignar(){
	var dep = $("#idtick").val();
	$.ajax({
		type:"POST",
		url:"seleccionUsuarios.php",
		data:{dep : dep},
		success: function(data){
			$('#Usuariosporasignar').html(data);
		}
	});
}
function DepartamentoAsig(){
var dep = $("#idtick").val();
	$.ajax({
		type:"POST",
		url:"seleccionDepartamento.php",
		data:{dep:dep},
		success: function(data){
			$('#DepartamentoAsig').html(data);
		}
	});
}
function asignaciondeTickets(id){
	$.ajax({
		type:"POST",
		url:"asignandoUsuario.php",
		data:{iduser : id,
				idticket : $('#idtick').val()},
		success: function(data){
			if (data==1) {
				Swal.fire({
				  title: 'Seguro desea hacer esta asignación?',
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si'
				}).then((result) => {
				  if (result.value) {
				    Swal.fire(
				      'Asignación exitosa',
				      '',
				      'success'
				    )
		 		pagina();
                $("#mensajeshisto").animate({ scrollTop: $('#mensajeshisto').prop("scrollHeight")}, 1000);
					
					
				  }
				})
				}else{
					Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'Error',
						showConfirmButton: false,
						timer: 1500
					});
				}
		}
	});
}
function asignaciondeTicketsD(id){
	$.ajax({
		type:"POST",
		url:"asignandoDepartamento.php",
		data:{iddep : id,
				idticket : $('#idtick').val()},
		success: function(data){
			if (data==1) {
				Swal.fire({
				  title: 'Seguro desea hacer esta asignación?',
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si'
				}).then((result) => {
				  if (result.value) {
				    Swal.fire(
				      'Asignación exitosa',
				      '',
				      'success'
				    )
		 		pagina();
                $("#mensajeshisto").animate({ scrollTop: $('#mensajeshisto').prop("scrollHeight")}, 1000);
					
					
				  }
				})
				}else{
					Swal.fire({
			    		position: 'center-top', 
						icon: 'error',
						title: 'Error',
						showConfirmButton: false,
						timer: 1500
					});
				}
		}
	});
}

function busquedainfocliente(dato){
var url = "busqueda.php"
	$.ajax({
		type:'POST',
		url:url,
		data:'rif='+dato,
			success:function(e){
				if (e==0) {
					$('#infor').hide();
					$("#brif").addClass(" is-invalid");
					Swal.fire({
					  icon: 'error',
					  title: 'Error',
					  text: 'Este cliente no se encuentra en la base de datos.',
					 // footer: '<a href>Why do I have this issue?</a>'
					})
				}
			 else 

				{
				;
				$('#CerrarTick').removeAttr("disabled");
				$("#brif").removeClass(" is-invalid");
				$("#brif").addClass(" is-valid");
				$('#infor').show();
				$('#infor').html(e);
				$("#rif").val(dato);
				var razonsocial = $("#razonsocial").val();
				var factura = $("#factura").val();
				$("#rs").val(razonsocial);

				$("#PersonaContac").val($("#rlegal").val());
				$("#Correo").val($("#correorl").val());
				$("#NumeroTelf1").val($("#telf1").val());
				$("#NumeroTelf2").val($("#telf2").val());
				var dato2 = $('#RIFCONSULTA').val();
					$.ajax({
					type: 'POST',
					url: 'selectserial.php',
					data:{dato2: dato2},
					success: function(e){
						$('#optseriales').html(e);
					}
				});

				if (factura!='' || factura== null ) {
					$("#BotonesCreadores").show();
					
				}else{
					$("#BotonesCreadores").hide();
				}
				$.ajax({
					type:'POST',
					url: 'consutaltickets.php',
					data:{busqueda: $("#busqueda").val()},
					success: function(data){
						if (data>0) {
							$("#tickClie").show();
					
						}else{
							$("#tickClie").hide();
						}
					}
				})
					$.ajax({
					url:'SeleccionDep.php',
					success: function(data){
						$('#asignarDep').html(data);
					}
				});
			}
		}
	});
};

function enviarmensaje(nombre, empresa, ticket, motivo, accion,tipoticket, correo) {
	$.ajax({
		type: 'POST',
		url: '../phpmailer/enviar.php',
		data:{	NB: nombre,
				EM: empresa,
				TK: ticket,
				MT: motivo,
				AC: accion,
				TPTK: tipoticket,
				Correo: correo}

	});
};
function datospassword(usuarioc,passwordc){
cadena="usuario=" + usuarioc +
          "&password=" + passwordc;
          $.ajax({
					type:'POST',
					url: 'password.php',
					data:cadena,
					success: function(data){
						if (data>0) {
							$("#tickClie").show();
					
						}else{
							$("#tickClie").hide();
						}
					}
				})
}

function cambiarestaus(){ 
		var estatus=$('#estatusTick').val();
		if (estatus==2) {
			$("#OpcEstatus").html(' <a  class="dropdown-item" data-toggle="modal" style="cursor: pointer" data-target="#modalcerrar">Cerrado</a>'); 
		
		} else if(estatus==1){
		$("#OpcEstatus").html('<a class="dropdown-item" style="cursor: pointer" onclick="cambiarestatus(2);">Proceso</a>'+
			' <a  class="dropdown-item" data-toggle="modal" style="cursor: pointer" data-target="#modalcerrar">Cerrado</a>'); 
		}
	}